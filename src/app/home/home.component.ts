import { Component, OnInit, OnDestroy } from '@angular/core';

export interface User {
  id: number;
  wallet: number;
  name: any;
  email: string;
  birthDate: Date;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
  users:User[] = [
    {
      id: 1,
      name: 'Budi Doremi',
      birthDate: new Date(),
      wallet: 2000000,
      email: 'Budi@mail.com'
    },
    {
      id: 2,
      name: 'John',
      birthDate: new Date(),
      wallet: 2000000,
      email: 'John@mail.com'
    },
    {
      id: 3,
      name: 'Kevin',
      birthDate: new Date(),
      wallet: 2000000,
      email: 'Kevin@mail.com'
    },
  ]

  constructor() {
  }

  ngOnInit(): void {
    console.log('ini di on init');
  }

  submit(value:string) {
    this.users.push(
      {
        name: value,
        email: '',
        id: 10,
        birthDate: new Date(),
        wallet: 0
      }
    )
    console.log(value, '<< value from child');
    
  }
}
