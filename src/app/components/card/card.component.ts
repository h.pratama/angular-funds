import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from 'src/app/home/home.component';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
  @Input() user:User = {
    id: 0,
    name: '',
    email: '',
    wallet: 0,
    birthDate: new Date(),
  }

  @Output() setDataToParent = new EventEmitter();


  constructor() { }

  ngOnInit(): void {
  }

  sendToParent(val:string) {
    this.setDataToParent.emit(val)
  }

}
